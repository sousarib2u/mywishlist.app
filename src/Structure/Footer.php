<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 10/12/2018
 * Time: 13:33
 */

namespace wishlist\Structure;


/**
 * Class Footer gérant le code html du footer
 * @package wishlist\Structure
 */
class Footer {

    /**
     * affichager du footer
     * @return string html
     */

    public static function getFooter()
    {

        $html = '
        <footer>
            <p>Projet de Programmation web dans le cadre des études en DUT Informatique à l\'IUT Nancy Charlemagne.</p>
            <ul>
                <li>SOUSA RIBEIRO Pedro</li>
                <li>FELIX Léo</li>
                <li>JURCZAK Maxime</li>
                <li>KIRCH Dylan</li>
            </ul>
        </footer>
    </body>
</html>';
        return $html;
    }
}