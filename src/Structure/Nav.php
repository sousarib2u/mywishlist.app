<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 18/12/2018
 * Time: 10:46
 */

namespace wishlist\Structure;


/**
 * Class Nav gérant le code html du menu
 * @package wishlist\Structure
 */
class Nav {

    /**
     * affiche le menu
     * @return string
     */
    public static function getNav() {
        if(isset($_SESSION['prenom'])){
            $user ="<li><a href='/mylists'> Mes listes</a></li>
            <li><a href='/disconnect'>Se déconnecter</a></li>
            <li><a href='/myaccount'>Espace Personnel</a></li>";
        } else {
            $user = "<li><a href='/login'>Se connecter</a></li>
                    <li><a href ='/sign'>S'inscrire</a></li>";

        }
        $html = '
        <nav>
            <ul>
                <li>' . '<a href="/">Accueil</a>' . '</li>
                <li><a href="/list/">Listes publiques</a></li>' . $user .
            '<li><a href="/create">Créer une liste</a></li>' .
            '</ul>
        </nav>
        ';
        return $html;
    }

}