<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 10/12/2018
 * Time: 11:13
 */

namespace wishlist\Structure;


/**
 * Class Head gérant le code html du Head
 * @package wishlist\Structure
 */
class Head {

    /**
     * affiche le head
     * @param string $title
     * @param String $css
     * @return string html
     */

    public static function getHead(string $title, String $css)
    {

        $html = '
<!DOCTYPE Html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
            <link href=' . $css . ' rel="stylesheet" type="text/css"/>
        <link rel="icon" href="../img/favicon.ico" />
        <title>' . $title . '</title>
    </head>';
        return $html;
    }

    /** private static function cssLink(array $css){
     * $cssLink = '';
     * foreach ($css as $c){
     * $cssLink = $cssLink.'
     * <link href="/css/style.css" rel="stylesheet" type="text/css"/>';
     * }
     * return $cssLink;
     * }**/
}