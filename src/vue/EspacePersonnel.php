<?php
/**
 * Created by PhpStorm.
 * User: leo
 * Date: 18/12/18
 * Time: 11:32
 */

namespace wishlist\vue;


use wishlist\models\Utilisateur;

/**
 * class gérant les methodes concernants l'espace personnnel
 * Class EspacePersonnel
 * @package wishlist\vue
 */

class EspacePersonnel
{


    /**
     * affiche l'espace personnel
     * @return string html
     */
    public static function AfficherEspacePersonnel()
    {

        $user = Utilisateur::where('user_id', '=', $_SESSION['user_id'])->first();
        $html = '
    <div class="espace_perso">
        <h2>Information sur le compte</h2>
        <div class="info_compte">
            <div class="info_id">
                <h3>Informations identifiants</h3>
                <p><strong>E-mail:</strong> ' . $user->adresse . '</p>
                <p><strong>Mot de passe:</strong> <i>-- caché --</i></p>
            </div>
            <div class="info_general">
                <h3>Informations Générales</h3>
                <p><strong>Nom:</strong> ' . $user->nom . '</p>
                <p><strong>Prénom:</strong> ' . $user->prenom . '</p>
            </div>
        </div>
        <div class="bt"><a href="/myaccount/modify">Modifier mes informations</a></div>
    </div>
        ';

        return $html;

    }

    /**
     * affiche un formulaire de modification des informations personnels
     * @return string html
     */
    public static function ModifierInformations()
    {
        $user = Utilisateur::where('user_id', '=', $_SESSION['user_id'])->first();

        $html = '
        <form action="../myaccount/validate" method ="post">
            <div class="espace_perso">
                <h2>Information sur le compte</h2>
                <div class="info_compte">
                    <div class="info_id">
                        <h3>Informations identifiants</h3>
                        <p><strong>E-mail:</strong> <input name="mail" type="email" required value="' . $user->adresse . '"></p>
                        <p><strong>Ancien mot de passe:</strong> <input name="old_pass" type="password"></p>
                        <p><strong>Nouveau mot de passe:</strong> <input name="new_pass" type="password" minlength="8"></p>
                        <p><strong>Confirmer mot de passe:</strong> <input name="confirm_pass" type="password" minlength="8"></p>
                    </div>
                    <div class="info_general">
                        <h3>Informations générales</h3>
                        <p><strong>Nom:</strong> <input name="new_nom" type="text" required value="' . $user->nom . '"></p>
                        <p><strong>Prénom:</strong> <input name="new_prenom" type="text" required value="' . $user->prenom . '"</p>
                    </div>
                </div>
                <div class="bt"><input type="submit" value="Enregistrer mes nouvelles informations"></div>
         </form>
                <div class="warning">
                    <form action="../myaccount/delete" method ="post">
                        <p>/!\  ATTENTION  /!\ <br> La suppression du compte entrainera la suppression de toutes les listes ainsi que toutes les données liées au compte. La suppression sera irréverssible !</p>
                        <input type="submit" value="Je comprend l\'avertissement et je supprime mon compte">
                    </form>
                </div>
            </div>
        
        ';

        return $html;
    }

    /**
     * supprime dans la base de données un utilisateur
     */
    public static function SupprimerUtilisateur()
    {
        $util = Utilisateur::where('user_id', '=', $_SESSION['user_id'])->first();
        //TODO supprimer tout ce qui est en rapport de l'utilisateur
        $com = \wishlist\models\CommentaireListe::select()->where('user_id', '=', $util->user_id);
        $liste = \wishlist\models\Liste::select()->where('user_id', '=', $util->user_id);
        $reserve = \wishlist\models\ReservationItem::select()->where('user_id', '=', $util->user_id);

        $user = \wishlist\models\Utilisateur::select()->where('user_id', '=', $util->user_id);

        $reserve->delete();
        $com->delete();
        $liste->delete();
        $user->delete();


        unset($_SESSION['user_id']);
        unset($_SESSION['nom']);
        unset($_SESSION['prenom']);
        unset($_SESSION['email']);

        header("Location: ../../");
        die();
    }


    /**
     * sauvegarde les modifications de l'utilisateur
     */
    public static function ValiderModifications()
    {
        $user = Utilisateur::where('user_id', '=', $_SESSION['user_id'])->first();
        if (!empty($_POST['old_pass'])) {
            if (password_verify($_POST['old_pass'], $user->password)) {
                if ($_POST['new_pass'] === $_POST['confirm_pass']) {
                    $user->password = password_hash($_POST['new_pass'], PASSWORD_DEFAULT);
                }
            }
        }
        $user->adresse = filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL);;
        $user->nom = filter_var($_POST['new_nom'], FILTER_SANITIZE_SPECIAL_CHARS);
        $user->prenom = filter_var($_POST['new_prenom'], FILTER_SANITIZE_SPECIAL_CHARS);
        $user->save();
        header("Location: ../../myaccount");
        die();
    }
}