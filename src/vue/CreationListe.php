<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 09/01/19
 * Time: 09:01
 */

namespace wishlist\vue;


use wishlist\models\Liste;

/**
 * Class CreationListe gérant la création de liste
 * @package wishlist\vue
 */
class CreationListe
{
    /**
     * affiche un formulaire de création de liste
     * @return string html
     */
    public static function TypeListe()
    {
        $form = "";
        $choix = "<input type='checkbox' id='prive' name='prive' checked>
                        <label for='prive'>Liste Privée</label>";
        if (isset($_SESSION['user_id'])) {
            $form = $form . "<h3>Créez une liste</h3>";
            $choix = $choix . "<div>
                                 <input type='radio' id='anon' name='list' value='anon' checked>
                                 <label for='anon'>Anonyme</label>
                             </div>
                             <div>
                                 <input type='radio' id='perso' name='list' value='perso' checked>
                                 <label for='conn'>Personnelle</label>
                             </div>";
        } else {
            $form = $form . "<h3>Créez une liste anonyme. Pour une liste personnelle, connectez-vous.</h3>";
        }
        $form = "
        <div class='formulaire'>
            " . $form . "
            <form action='/validatelist ' method='post'>
                <span>Nom de la liste</span><input type='text' name= 'titre' placeholder='Ma liste' required><br>
                <span>description</span><input type='text' name= 'description' placeholder='description' required><br>
                <span>date d'expiration</span><input type='date' name= 'date' required><br>";

        $choix = $choix . "<div class='bt'><input type='submit' value='Valider'></div></div></form>";
        return $form . $choix;
    }

    /**
     * sauvegarde dans la base de données la création d'une nouvelle liste
     * @return string
     * @throws \Exception
     */
    public static function CreerListe()
    {
        $liste = new Liste();
        $liste->titre = filter_var($_POST['titre'], FILTER_SANITIZE_SPECIAL_CHARS);
        $liste->description = filter_var($_POST['description'], FILTER_SANITIZE_SPECIAL_CHARS);
        $liste->expiration = filter_var($_POST['date'], FILTER_SANITIZE_SPECIAL_CHARS);
        if (isset($_SESSION['user_id']) && ($_POST['list'] == 'perso')) {
            $liste->user_id = $_SESSION['user_id'];
        } else {
            $liste->user_id = 0;
        }
        if (isset($_POST['prive'])) {
            $token = bin2hex(random_bytes(15));
            $liste->token = $token;
        } else {
            $tkmax = 0;
            $list = Liste::all();
            foreach ($list as $tk) {
                if (substr($tk->token, 0, 8) == 'nosecure') {
                    $tkact = substr($tk->token, 8);

                    if ($tkmax <= $tkact) {
                        $tkmax = $tkact + 1;
                    }

                }
                $token = 'nosecure' . $tkmax;
                $liste->token = $token;
            }

        }
        $liste->save();
        $html = "<h1>Votre nouvelle liste a été crée ! Veuillez ajouter un objet dès maintenant !</h1>
                 <h2>Notez bien ce code d'accès !</h2>
                 <h2>$token</h2>";
        return $html . AfficheurItem::FormulaireAjoutItem($token);
    }
}