<?php
/**
 * Created by PhpStorm.
 * User: leo
 * Date: 18/12/18
 * Time: 11:34
 */

namespace wishlist\vue;

/**
 * Class Accueil gérant les méthodes d'affichage de l'accueil
 * @package wishlist\vue
 */
class Accueil
{

    /**
     * affiche l'accueuil
     * il y a un bouton qui permet de chercher une liste selon une adresse
     * @return string html
     */
    public static function AfficherAccueil()
    {
        $app = \Slim\Slim::getInstance();
        $texte = "<h1>Bienvenue sur My WishList !</h1>";
        $texte = $texte . "<p class='desc_site'>MyWishList.app est une application en ligne pour créer, partager et gérer des listes de cadeaux.
L'application vous permet de créer une liste de souhaits à l'occasion d'un événement
particulier (anniversaire, fin d'année, mariage, retraite …) et permet de diffuser cette liste de
souhaits à un ensemble de personnes concernées. Ces personnes peuvent alors consulter cette liste
et s'engager à offrir un ou plusieurs éléments de la liste. Ces éléments seront alors marqué comme réservé dans cette
liste.</p>";
        $form = "<div class='formulaire'>
                <form action = '/redirect/search' method= 'post'>
                <span>Accèder à une liste</span><input type='text' name= 'token' placeholder=\"Code d'accès\" required><br>
                <div class='bt'><input type='submit' value='Valider'></div></form></div>";
        $html = $texte . $form;
        return $html;
    }

}