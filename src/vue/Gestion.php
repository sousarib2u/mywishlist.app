<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 05/01/19
 * Time: 11:24
 */

namespace wishlist\vue;

/**
 * Class Gestion gérant la deconnexion
 * @package wishlist\vue
 */
class Gestion
{

    /**
     * se deconnecter
     */
    public static function Disconnect()
    {
        session_unset();
        header("Location: index.php");
        die();
    }
}