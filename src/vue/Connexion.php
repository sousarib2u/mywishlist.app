<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 05/01/19
 * Time: 11:37
 */

namespace wishlist\vue;

/**
 * Class Connexion gérant les méthodes de connexion
 * @package wishlist\vue
 */
class Connexion
{
    /**
     * affiche le formulaire de connexion
     */
    public static function Login()
    {
        $form = "
            <div class='formulaire'>
                <h3>Formulaire de connexion</h3>
                <form action='/validatelog' method='POST'>
                    <span>Adresse mail: </span><input type='email' name= 'email' placeholder='E-Mail' required><br>
                    <span>Mot de passe: </span><input type='password' name = 'password' required><br>
                    <div class='bt'><input type='submit' value='Valider'></div>
                </form>
            </div>";
        echo $form;
    }

    /**
     * verifie si le couple identifiant/mdp sont conforme à la base de données avec
     * l'identifiant et le mdp entrée en paramètre
     * @param $em String
     * @param $pass String
     */
    public static function ValidationConn($em, $pass)
    {
        $check = \wishlist\models\Utilisateur::select('user_id')->where('adresse', '=', $em)->first();
        if (isset($check) && $check->user_id > 0) {
            $passsql = \wishlist\models\Utilisateur::select('password')->where('adresse', '=', $em)->first();
            $pass = password_verify($pass, $passsql->password);

            if ($pass) {
                $connected = \wishlist\models\Utilisateur::select('nom', 'prenom')->where('adresse', '=', $em)->first();
                $_SESSION['nom'] = $connected->nom;
                $_SESSION['prenom'] = $connected->prenom;
                $_SESSION['user_id'] = $check->user_id;
                $_SESSION['email'] = $em;
                header("Location: index.php");
                die();

            } else {
                echo "<p class='erreur'>L'addresse ou le mot de passe est invalide</p>";
                self::Login();

            }
            unset($temp);
        } else {
            echo "<p class='erreur'>Un problème s'est produit, veuillez réessayer. Si le problème persiste, contactez l'adminitrateur du site</p>";
            self::Login();
        }
    }
}