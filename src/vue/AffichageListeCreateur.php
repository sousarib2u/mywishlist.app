<?php
/**
 * Created by PhpStorm.
 * User: leo
 * Date: 18/12/18
 * Time: 11:34
 */

namespace wishlist\vue;

use wishlist\models\CommentaireListe;
use wishlist\models\Utilisateur;


/**
 * Class AffichageListeCreateur gérant l'affichage et la modification des listes privés
 * @package wishlist\vue
 */
class AffichageListeCreateur{


    /**
     * affiche tous les messages selon une liste entrée en paramètre
     * @param $tk token de la liste
     * @return string html
     */
    public static function AfficherMessagesListe($tk)
    {
        $tk = filter_var($tk, FILTER_SANITIZE_SPECIAL_CHARS);
        $liste = \wishlist\models\Liste::select()->where('token', '=', $tk)->first();
        $mes = \wishlist\models\CommentaireListe::select('message')->where('no', '=', $liste->no)->first();
        $req = "<p>$mes->message</p>";
        $html = $req;
        return $html;
    }


    /**
     * affiche un formulaire de modification des informations de la liste
     * @param $tk token de la liste
     * @return string html
     */
    public static function ModifierListePerso($tk)
    {
        $tk = filter_var($tk, FILTER_SANITIZE_SPECIAL_CHARS);
        if ($_SESSION['user_id']) {
            $val = $_SESSION['user_id'];
            $req = "";
            $liste = \wishlist\models\Liste::select()->where('token', '=', $tk)->first();

            if (($val == $liste->user_id) && ($liste->user_id != 0)) {
                $req = "
            <div class='formulaire'> 
                <h3>Modifier la liste</h3> 
                <form action='/mylists/validate/$liste->no' method ='post'>
                    <span>Titre: </span><input type='text' name = 'titre' placeholder='Titre...' value=\"$liste->titre\" required><br>
                    <span>Description: </span><input type='text' name = 'description' placeholder='Description...' value=\"$liste->description\" required ><br>
                    <span>Date d'expiration: </span><input type='date' name = 'expiration' placeholder='Expiration...' value=\"$liste->expiration\" required><br>
                    <input type='submit' value='Valider'>
                </form>
            </div>
                 ";
            } else {
                echo "<p class='erreur'>Vous n'avez pas accès à cette liste</p>";
            }

        }
        $html = $req;
        return $html;

    }

    /**
     * sauvegarde les modifications dans la base de données
     * @param $no numero de la liste
     */
    public static function ValiderModification($no)
    {
        $list = \wishlist\models\Liste::select()->where('no', '=', $no)->first();
        $list->titre = filter_var($_POST['titre'], FILTER_SANITIZE_SPECIAL_CHARS);
        $list->description = filter_var($_POST['description'], FILTER_SANITIZE_SPECIAL_CHARS);
        $list->expiration = filter_var($_POST['expiration'], FILTER_SANITIZE_SPECIAL_CHARS);
        $list->save();
        header("Location: ../../mylists");
        die();
    }

    /**
     * supprime la liste dans la base de données
     * @param $no numero de la liste
     */
    public static function SupprimerListe($no)
    {
        $list = \wishlist\models\Liste::select()->where('no', '=', $no)->first();
        $list->delete();
        header("Location: ../../mylists");
        die();
    }


}