<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 2018-12-12
 * Time: 11:45
 */

namespace wishlist\vue;

use wishlist\models\CommentaireListe;
use wishlist\models\Liste;
use wishlist\models\Utilisateur;

/**
 * Class AfficheurListe gérant les affichages, les modification et les ajouts des listes
 * @package wishlist\vue
 */
class AfficheurListe
{
    /**
     * affiche toutes les listes avec un boolean entrée en paramètre
     * @param $perso boolean
     * @return String html
     */
    public static function AfficherToutesListes($perso)
    {
        $perso = filter_var($perso, FILTER_SANITIZE_SPECIAL_CHARS);
        if ($perso == false) {
            $tbl = Liste::where('token', 'like', 'nosecure%')->get();
        } else {
            $val = $_SESSION['user_id'];
            $tbl = Liste::get()->where('user_id', '=', $val);
        }

        $html = '';
        foreach ($tbl->reverse() as $liste) {
            $html = $html . '
                ' . self::ListView($liste->no, $perso, true) . '
            ';
        }

        return $html;
    }

    /**
     * génère le code html d'une liste
     * @param $no entier identifiant la liste à générer
     * @param $perso boolean indiquant si la liste se trouve dans une vue personnelle (true) ou publique (false)
     * @return string le code html généré
     */
    private static function ListView($no, $perso, $msg)
    {
        $liste = Liste::where('no', '=', $no)->first();

        if ($liste->user_id >= 1) {
            $user = Utilisateur::where('user_id', '=', $liste->user_id)->first();
            $utilisateur = $user->nom . ' ' . $user->prenom;
        } else {
            $utilisateur = 'Anonyme';
        }
        $tk = $liste->token;
        $html = '
        <div class="list">
            <div class="content">
                <ul>
                    <li class="nom_list"><strong>' . $liste->titre . '</strong><br><div class="user_liste">par ' . $utilisateur . '</div></li>
                    <li class="desc_list">' . $liste->description . '<br> Expiration: ' . $liste->expiration . '</li>
                    <li class="bt"><a href="../list/retrieve?tk=' . $tk . '">Afficher les éléments</a></li>
               ';

        if ($msg == true) {
            $html = $html . '
                    <li class="bt"><a href="../mylists/messages?tk=' . $tk . '">Messages</a></li>
            ';
        }

        if ($perso == true) {
            $html = $html . '
                    <li class="bt"><a href="../list/retrieve?tk=' . $tk . '">Partager</a></li>
                    <li class="bt"><a href="../mylists/edit?tk=' . $tk . '">Modifier</a></li>
                    <li class="bt"><a href="../mylists/delete?tk=' . $tk . '">Supprimer</a></li>
            ';
        }

        $html = $html . '
                </ul>
            </div>
        </div>';

        return $html;
    }


    /**
     * affiche les commentaires selon le numéro de liste
     * @param $no
     * @return String html
     */
    public static function AfficherMessageListes($tk)
    {
        $tk = filter_var($tk, FILTER_SANITIZE_SPECIAL_CHARS);
        $list = Liste::where('token', '=', $tk)->first();
        $messages = CommentaireListe::where('no', '=', $list->no)->get();

        $html = self::ListView($list->no, false, false);
        $rendu_msg = '
        <div class="list_msg">
            <form action="/msg/validate?tk=' . $tk . '", method="post">
                <textarea name="msg" rows="2" placeholder="Entrez votre message ici..."></textarea>
                <input type="submit" value="Envoyer">
            </form>
        ';

        foreach ($messages->reverse() as $msg) {
            if ($msg->user_id > 0) {
                $user = Utilisateur::where('user_id', '=', $msg->user_id)->first();
                $author = $user->nom . ' ' . $user->prenom;
            } else {
                $author = 'Anonyme';
            }

            $rendu_msg = $rendu_msg . '
            <hr size="1px" width="95%">
            <div class="msg">
                <strong>' . $author . ' dit:</strong>
                <p>' . $msg->message . '</p>
            </div>
            ';
        }

        $rendu_msg = $rendu_msg . '
        </div>
        ';

        return $html . '
        ' . $rendu_msg . '
        ';
    }

    /**
     * Permet de valider le message entré par un utilisateur
     */
    public static function ValiderMessage($tk)
    {
        if (isset($_SESSION['user_id'])) {
            $user = Utilisateur::where('user_id', '=', $_SESSION['user_id'])->first();
        } else {
            $user = Utilisateur::where('user_id', '=', '0')->first();
        }

        $liste = Liste::where('token', '=', $tk)->first();

        $msg = new CommentaireListe();
        $msg->message = filter_var($_POST['msg'], FILTER_SANITIZE_SPECIAL_CHARS);
        $msg->user_id = $user->user_id;
        $msg->no = $liste->no;
        $msg->save();
        unset($_POST);
        header("Location: ../../mylists/messages?tk=$tk");
        die();
    }

}