<?php
/**
 * Created by PhpStorm.
 * User: leo
 * Date: 18/12/18
 * Time: 11:37
 */

namespace wishlist\vue;

use wishlist\models\Item;
use wishlist\models\Liste;
use wishlist\models\ReservationItem;

/**
 * Class AfficheurItem gérant les affichages, les modification et les ajouts des items
 * @package wishlist\vue
 */
class AfficheurItem{

    /**
     * affiche un item selon une id_item entrée en paramètre
     * @param $id item_id
     * @return string html
     */

    public static function AfficherInfoItem($id)
    {
        $item = \wishlist\models\Item::where('item_id', '=', $id)->first();
        if (substr($item->img, 0, 4) == 'http') {
            $image = $item->img;
        } else {
            $image = '../img/' . $item->img;
        }

        $html = "<title>$item->nom</title>" .
            "<p>$item->descr</p>" .
            "<img src=" . "$image>" .
            "<p>$item->tarif</p>" .
            "<td><a href =" . "/list/retrieve/edit/$item->item_id" . " >Modifier</a></td>
         <form action='/list/retrieve/edit/delete/$item->item_id' method ='post'>
         <input type='submit' value='Supprimer'>
         </form>";

        return $html;
    }

    /**
     * affiche un formulaire de modification des informations selon une id_item entrée en paramètre
     * @param $id item_id
     * @return string html
     */
    public static function ModifierItem($id)
    {
        $item = \wishlist\models\Item::select()->where('item_id', '=', $id)->first();
        $html = "
        <div class='formulaire'>
            <h3>Modification de l'item</h3>
            <form action='/list/retrieve/edit/validate/$item->item_id' method ='post'>
                <span>Nom: </span><input type='text' name='nom' placeholder='nom' value=\"$item->nom\" required><br>
                <span>Description: </span><input type='text' name='descr' placeholder='descr' value=\"$item->descr\" required ><br>
                <span>Lien externe: </span><input type='url' name='url' placeholder='url' value=\"$item->url\" required><br>
                <span>Url Image: </span><input type='url' name='img' placeholder=\"URL de l'image\" value=$item->img><br>
                <span>Fichier image: </span><input type='file' name='imageup' accept='image/jpg, image/jpeg, image/png'><br>
                <span>Tarif: </span><input type='number' name='tarif' placeholder='tarif' min='0' value='$item->tarif' required><br>
                <input type='submit' value='Valider'>
            </form>
        </div>
        ";

        return $html;
    }

    /**
     * sauvegarde les modifications dans la base de données de l'item avec une id_item
     * entrée en paramètre
     * @param $id item_id
     */
    public static function ValiderModification($id)
    {
        $item = \wishlist\models\Item::select()->where('item_id', '=', $id)->first();
        $liste = Liste::select()->where('no', '=', $item->liste_id)->first();
        $token = $liste->token;
        $item->nom = filter_var($_POST['nom'], FILTER_SANITIZE_SPECIAL_CHARS);
        $item->descr = filter_var($_POST['descr'], FILTER_SANITIZE_SPECIAL_CHARS);
        $item->url = filter_var($_POST['url'], FILTER_SANITIZE_URL);
        $item->tarif = filter_var($_POST['tarif'], FILTER_SANITIZE_NUMBER_FLOAT);
        if ($_POST['img'] != '' && !empty($_FILES['imageup']['name'])) {
            echo "<p class= 'erreur'>Une seule image est autorisée</p>" . self::FormulaireAjoutItem($id);

        } else {

            if ($_POST['img'] != '' && empty($_FILES['imageup']['name'])) {
                $item->img = filter_var($_POST['img'], FILTER_SANITIZE_URL);

            } elseif (!empty($_FILES['imageup']['name']) && $_POST['img'] == '') {
                $folder = 'img/';
                echo $folder;
                $file = basename($_FILES['imageup']['name']);
                if (move_uploaded_file($_FILES['imageup']['tmp_name'], $folder . $file)) {
                    echo "Uploaded";
                    $item->img = $file;
                } else {
                    echo "Failed !";
                }
            }
            $item->save();
            header("Location: /list/retrieve?tk=" . $token);
            die();
        }
    }


    /**
     * supprime l'item dans la base de données avec une id_item entrée en paramètre
     * @param $id
     */
    public static function SupprimerItem($id)
    {
        $list = \wishlist\models\Item::select()->where('item_id', '=', $id)->first();
        $list->delete();
        header("Location: /mylists");
        die();
    }

    /**
     * affiche un formmulaire d'ajout d'item avec un token de liste en paramètre
     * @param $tk
     * @return string html
     */
    public static function FormulaireAjoutItem($tk)
    {
        $html = '';
        $liste = Liste::select()->where('token', '=', $tk)->first();

        if ($liste->user_id == 0 && substr($liste->token, 0, 8) == 'nosecure') {
            $html = "<p class='erreur'>Attention, une liste anonyme publique ne pourra être modifiée après la validation !</p>";
        }
        $html = $html . "
        <div class='formulaire'>
            <h3>Ajouter un item</h3>
            <form action='/list/add/retrieve/$tk' method ='post' enctype=\"multipart/form-data\">
                <span>Nom: </span><input type='text' name='nom' placeholder='Nom...' required><br>
                <span>Description: </span><input type='text' name='descr' placeholder='Description...' required><br>
                <span>Lien externe: </span><input type='url' name='url' placeholder='Lien externe...' required><br>
                <span>URL Photo:</span><input type='url' name='img' placeholder='URL de l'image' ><br>
                <input type='hidden' name='MAX_FILE_SIZE' value='10000000'>
                <span><input type='file' name ='imageup' accept= 'image/jpg, image/jpeg, image/png'</span><br>
                <span>Tarif: </span><input type='number' name='tarif' placeholder='Tarif...' min='0' required><br>
                
                <input type='submit' value='Enregistrer'>
               
             </form>
        </div>
        ";

        return $html;
    }

    /**
     * ajout un item dans la base de données avec un numero de liste entrée en paramètre
     * @param $id numero de liste (no)
     * @return string html
     */
    public static function AjoutItem($id)
    {
        $html = '';
        $liste = \wishlist\models\Liste::select()->where('token', '=', $id)->first();
        $item = new Item();
        $item->liste_id = $liste->no;
        $item->nom = filter_var($_POST['nom'], FILTER_SANITIZE_SPECIAL_CHARS);
        $item->descr = filter_var($_POST['descr'], FILTER_SANITIZE_SPECIAL_CHARS);
        $item->url = filter_var($_POST['url'], FILTER_SANITIZE_SPECIAL_CHARS);
        $item->tarif = filter_var($_POST['tarif'], FILTER_SANITIZE_SPECIAL_CHARS);

        if ($_POST['img'] != '' && !empty($_FILES['imageup']['name'])) {

            echo "<p class= 'erreur'>Une seule image est autorisée</p>" . self::FormulaireAjoutItem($id);

        } else {

            if ($_POST['img'] != '' && empty($_FILES['imageup']['name'])) {
                $item->img = $_POST['img'];

            } elseif (!empty($_FILES['imageup']['name']) && $_POST['img'] == '') {
                $folder = 'img/';
                echo $folder;
                $file = basename($_FILES['imageup']['name']);
                if (move_uploaded_file($_FILES['imageup']['tmp_name'], $folder . $file)) {
                    echo "Uploaded";
                    $item->img = $file;
                } else {
                    echo "Failed !";
                }
            }
            $item->save();
            $html = $html . self::FormulaireAjoutItem($liste->token) . "<p class='bt'><a href ='/list/retrieve?tk=$liste->token'>Valider et retourner à la liste</a></p>";
            return $html;
        }
    }


    /**
     * affiche tous les items d'une liste avec un token entrée en paramètre
     * @param $tk token
     * @return string html
     */
    public static function AfficherItemsListe($tk)
    {

        $html = "";

        $list = Liste::where('token', '=', $tk)->first();

        if (isset($list)) {

            $tbl = Item::where('liste_id', '=', $list->no)->get();


            $html = "<h1>$list->titre</h1>";
            if (isset($_SESSION['user_id'])) {
                if ($_SESSION['user_id'] == $list->user_id) {
                    $html = $html . "
            <div class='add_item'><a href =" . "../list/add/$tk" . " >+</a></div>";
                }
            }
            if ($list->user_id == 0 && substr($list->token, 0, 8) != 'nosecure') {
                $html = $html . "
            <div class='add_item'><a href =" . "../list/add/$tk" . " >+</a></div>";
            }

            foreach ($tbl as $item) {

                if (substr($item->img, 0, 4) == 'http') {
                    $image = $item->img;

                } else {
                    $image = '../img/' . $item->img;
                }
                $html = $html . "
            <div class='item_view'>
                <ul>
                    <li><img src=\"$image\"></li>
                    <li class='nom_item'>$item->nom</li>
                    <li class='desc_item'>$item->descr</li>
                    <li class='bt'><a href=\"$item->url\" target=\"_blank\">Lien</a></li>
                    ";
                if (isset($_SESSION['user_id'])) {

                    if ($list->user_id == $_SESSION['user_id']) {
                        $html = $html . "<li class='bt'><a href='/list/retrieve/edit/$item->item_id'>Modifier</a></li>";
                    }
                }
                $test2 = ReservationItem::where('item_id', '=', $item->item_id)->first();
                if (isset($test2)) {
                    $html = $html . "<li class='reserv_item'>Objet reservé !</li>";
                } else {
                    $html = $html . "<li class='bt'><a href =" . "../list/retrieve/reserve/$item->item_id" . ">Reservation</a></li>";
                }

                $html = $html . "<li>Prix: $item->tarif €</li>
                </ul>
            </div>";
            }
        } else {
            echo "<p class= 'erreur'>Cette liste n'existe pas</p>";
        }

        return $html;
    }

    /**
     * reserve un item dans la base de données avec un id_item entrée en paramètre
     * @param $id id_item
     * @return string html
     */
    public static function ReserverItem($id)
    {

        $html = "
        <div class='formulaire'>
            <h3>Réserver un item</h3>
            <form action='/list/validate/reservation/$id' method='post'>
                <span>Message: </span><input type='text' name= 'message' placeholder='Message...'><br>
                <div>
                    <input type='radio' id='anon' name='visible' value='anon' checked>
                    <label for='anon'>Anonyme</label>
                </div>
                <div>
                    <input type='radio' id='public' name='visible' value='public' checked>
                    <label for='conn'>Public</label>
                </div>
                <br>
        ";
        if (!isset($_SESSION['user_id'])) {
            $html = $html . "
                <span>Nom: </span><input type='text' name= 'nom' placeholder='Nom...'><br>
                <span>Prenom: </span><input type='text' name= 'prenom' placeholder='Prenom...'><br>
            ";
        }

        $html = $html . "
                <input type='submit' value='Valider'>
            </form>
        ";

        return $html;

    }

    /**
     * reserve un item dans la base de données avec un id_item en paramètre
     * @param $id id_item
     */
    public static function ReserverItemValider($id)
    {
        $reservation = new ReservationItem();
        if ($_POST['visible'] == 'public') {
            if (isset($_SESSION['nom'])) {
                $reservation->nom = filter_var($_SESSION['nom'], FILTER_SANITIZE_SPECIAL_CHARS);
            }
            if (isset($_SESSION['prenom'])) {
                $reservation->prenom = filter_var($_SESSION['prenom'], FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }

        if (isset($_POST['message'])) {
            $reservation->message = filter_var($_POST['message'], FILTER_SANITIZE_SPECIAL_CHARS);
        }

        if (isset($_SESSION['user_id']) && $_POST['visible'] == 'public') {
            $reservation->user_id = $_SESSION['user_id'];
        } else {
            $reservation->user_id = 0;
        }
        $reservation->item_id = $id;

        $reservation->save();
        header("Location: /list/");
        die();

    }

}