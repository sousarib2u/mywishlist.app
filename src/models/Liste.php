<?php
/**
 * Created by PhpStorm.
 * User: Felix
 * Date: 21/11/2018
 * Time: 08:59
 */

namespace wishlist\models;

class Liste extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'liste';
    protected $primaryKey = 'no' ;
    public $timestamps = false ;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * retourne une liste d'item de la liste
     */
    public function items(){
        return $this->hasMany('\wishlist\models\Item','liste_id');
    }

}