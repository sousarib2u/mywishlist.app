<?php
/**
 * Created by PhpStorm.
 * User: Felix
 * Date: 21/11/2018
 * Time: 08:58
 */

namespace wishlist\models;

class Item extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'item';
    protected $primaryKey = 'item_id';
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * retourne la liste de l'item
     */
    public function liste(){
        return $this->belongsTo('\wishlist\models\Liste','liste_id');
    }


}