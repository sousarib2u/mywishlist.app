<?php
/**
 * Created by PhpStorm.
 * User: leo
 * Date: 08/01/19
 * Time: 10:41
 */

namespace wishlist\models;


class CommentaireListe extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'CommentaireListe';
    protected $primaryKey = 'id_com';
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * retourne la liste du message
     */
    public function liste(){
        return $this->belongsTo('\wishlist\models\Liste','no');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * retourne l'utilisateur du message
     */
    public function user(){
        return $this->belongsTo('\wishlist\models\Utilisateur','liste_id');
    }
}