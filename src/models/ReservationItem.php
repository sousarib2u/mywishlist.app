<?php
/**
 * Created by PhpStorm.
 * User: leo
 * Date: 08/01/19
 * Time: 10:49
 */

namespace wishlist\models;

class ReservationItem extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'ReservationItem';
    protected $primaryKey = '[item_id, user_id]';
    public $timestamps = false;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * retourne l'item reserver
     */
    public function item(){
        return $this->belongsTo('\wishlist\models\Item','item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * retourne l'utilisateur qui a reserver
     */
    public function user(){
        return $this->belongsTo('\wishlist\models\Utilisateur','user_id');
    }

}
