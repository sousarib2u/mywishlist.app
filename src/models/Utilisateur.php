<?php
/**
 * Created by PhpStorm.
 * User: Felix
 * Date: 21/11/2018
 * Time: 08:59
 */

namespace wishlist\models;

class Utilisateur extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'utilisateur';
    protected $primaryKey = 'user_id' ;
    public $timestamps = false ;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * retourne une liste d'item de la liste
     */
    public function user(){
        return $this->hasMany('\wishlist\models\Utilisateur');
    }

}