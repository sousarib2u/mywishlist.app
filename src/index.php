
<?php
session_start();
require_once '../vendor/autoload.php';

echo \wishlist\Structure\Head::getHead("My WishList", '/html/CSS/style.css');
echo '<body>';
echo \wishlist\Structure\Nav::getNav();
use Illuminate\Database\Capsule\Manager as DB;
$db = new DB();
$db->addConnection(parse_ini_file("../conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();



$app = new Slim\Slim();

$app->get('/', function (){
    echo \wishlist\vue\Accueil::AfficherAccueil();
});

/**$app->get('/list/retrieve', function ($tk){
    echo \wishlist\vue\AfficheurListe::AfficherItemsListe($tk);
});
**/

$app->get('/list/', function (){
    echo \wishlist\vue\AfficheurListe::AfficherToutesListes(false);
});

$app->get('/mylists/', function (){
    echo \wishlist\vue\AfficheurListe::AfficherToutesListes(true);
});

$app->post('/redirect/:type',function($type){
    $app = \Slim\Slim::getInstance();
    if($type == 'search'){
        $app->redirect('/list/retrieve?tk='.$_POST['token']);
    }
});

$app->get('/list/retrieve', function (){
    $app = \Slim\Slim::getInstance() ;
    $numliste= $app->request()->get('tk');
    $numitem = $app->request()->get('itm');
    if(!isset($numitem)){
        echo \wishlist\vue\AfficheurItem::AfficherItemsListe($numliste);
    }elseif (isset($numliste)&& isset($numitem)){
        echo \wishlist\vue\AfficheurItem::AfficherInfoItem($numitem);
    }

})->name('accesliste');

$app->get('/list/retrieve/reserve/:id', function ($id){
    echo \wishlist\vue\AfficheurItem::ReserverItem($id);
});

$app->post('/list/validate/reservation/:id', function ($id){
    echo "salut";
    echo \wishlist\vue\AfficheurItem::ReserverItemValider($id);
});


$app->get('/list/retrieve/edit/:id', function ($id){
    echo \wishlist\vue\AfficheurItem::ModifierItem($id);
});

$app->post('/list/retrieve/edit/validate/:id', function ($id){
    echo \wishlist\vue\AfficheurItem::ValiderModification($id);
});

$app->post('/list/retrieve/edit/delete/:id', function ($id){
    echo \wishlist\vue\AfficheurItem::SupprimerItem($id);
});

$app->get('/list/add/:id', function ($id){
    echo \wishlist\vue\AfficheurItem::FormulaireAjoutItem($id);
});

$app->post('/list/add/retrieve/:id', function ($id){
    echo \wishlist\vue\AfficheurItem::AjoutItem($id);
});


$app->get('/mylists/edit', function (){
    $app = \Slim\Slim::getInstance();
    $numliste= $app->request()->get('tk');
    echo \wishlist\vue\AffichageListeCreateur::ModifierListePerso($numliste);
});

$app->get('/mylists/messages', function (){
    $app = \Slim\Slim::getInstance();
    $token = $app->request()->get('tk');
    echo \wishlist\vue\AfficheurListe::AfficherMessageListes($token);
});

$app->post('/msg/validate', function (){
    $app = \Slim\Slim::getInstance();
    $token = $app->request()->get('tk');
    echo \wishlist\vue\AfficheurListe::ValiderMessage($token);
});

$app->post('/mylists/validate/:id', function ($id){
    echo \wishlist\vue\AffichageListeCreateur::ValiderModification($id);
});

$app->post('/mylists/delete?tk=:id', function ($id){
    echo \wishlist\vue\AffichageListeCreateur::SupprimerListe($id);
});


$app->get('/sign', function(){
    echo \wishlist\vue\Inscription::Formulaire();

});
$app->get('/login', function (){
    echo \wishlist\vue\Connexion::Login();
});

$app->post('/validatesign', function (){
    \wishlist\vue\Inscription::ValidationInscr($_POST['email']);
});

$app->post('/validatelog', function (){
    \wishlist\vue\Connexion::ValidationConn(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL),filter_var($_POST['password'], FILTER_DEFAULT));
});

$app->get('/disconnect', function(){
    \wishlist\vue\Gestion::Disconnect();
});

$app->get('/myaccount', function(){
   echo \wishlist\vue\EspacePersonnel::AfficherEspacePersonnel();
});
$app->get('/myaccount/modify', function(){
    echo \wishlist\vue\EspacePersonnel::ModifierInformations();
});
$app->post('/myaccount/delete', function(){
    echo \wishlist\vue\EspacePersonnel::SupprimerUtilisateur();
});
$app->post('/myaccount/validate', function(){
    echo \wishlist\vue\EspacePersonnel::ValiderModifications();
});

$app->get('/create', function (){
    echo \wishlist\vue\CreationListe::TypeListe();
});
$app->post('/validatelist', function (){
    echo \wishlist\vue\CreationListe::CreerListe();
});

$app->run();


echo \wishlist\Structure\Footer::getFooter();

